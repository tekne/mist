use rand::SeedableRng;
use clap::App;
use sdl2::pixels::PixelFormatEnum;
use sdl2::render::TextureAccess;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use rand_xoshiro::Xoroshiro64Star;
use std::time::{Duration, SystemTime};
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::sync::Arc;
use rand::Rng;
use parking_lot::Mutex;

use mist::{SimulationBox, Particle};

const DEFAULT_HEIGHT: u32 = 600;
const DEFAULT_WIDTH: u32 = 800;
const DEFAULT_BRUSH_SIZE: u32 = 10;
const FPS_UPDATE_DURATION: u64 = 3;
const TPS_UPDATE_DURATION: u64 = 3;
const TICK_FACTOR: u64 = 5;
const TARGET_TICK_LEN_IN_MS: u64 = 15;

fn draw_pixels(b: &SimulationBox, buffer: &mut [u8]) {
    for (i, p) in b.particles().iter().enumerate() {
        let c = p.base_color();
        buffer[4*i] = c.a;
        buffer[4*i + 1] = c.b;
        buffer[4*i + 2] = c.g;
        buffer[4*i + 3] = c.r;
    }
}

fn tick_loop<R: Rng>(sim: Arc<Mutex<SimulationBox>>, mut rng: R) {

    let mut ticks = 0;
    let mut total_ticks = 0;
    let mut total_elapsed = Duration::from_secs(0);
    let mut elapsed_ticking = Duration::from_secs(0);
    let mut total_elapsed_ticking = Duration::from_secs(0);
    let mut start = SystemTime::now();

    loop {
        {
            let mut sim = sim.lock();
            let tick_start = SystemTime::now();
            sim.tick((0, -65536), &mut rng);
            let tick_time = tick_start.elapsed().unwrap_or(Duration::from_secs(0));
            elapsed_ticking += tick_time;
            total_elapsed_ticking += tick_time;
            let target_tick_len = Duration::from_millis(TARGET_TICK_LEN_IN_MS);
            if tick_time < target_tick_len {
                std::thread::sleep(target_tick_len - tick_time);
            }
        }

        ticks += 1;
        total_ticks += 1;

        let loop_time = SystemTime::now();
        let elapsed = loop_time.duration_since(start).unwrap_or(Duration::from_secs(0));
        if elapsed > Duration::from_secs(TPS_UPDATE_DURATION) {
            total_elapsed += elapsed;
            start = loop_time;
            let tps = (ticks as f64) / elapsed.as_secs_f64();
            let ttps = (ticks as f64) / elapsed_ticking.as_secs_f64();
            let avg_tps = (total_ticks as f64) / total_elapsed.as_secs_f64();
            let tavg_tps = (total_ticks as f64) / total_elapsed_ticking.as_secs_f64();
            ticks = 0;
            elapsed_ticking = Duration::from_secs(0);
            println!(
                "TPS = {:.2} (true {:.2}, average {:.2}, true average {:.2})",
                tps, ttps, avg_tps, tavg_tps
            );
        }
    }
}

fn main() {
    let matches = App::new("mist")
                          .version("0.0")
                          .author("Jad Ghalayini <jad.ghalayini@mail.utoronto.ca>")
                          .about("Particle simulator")
                          .args_from_usage(
                              "-h, --height=[px] 'Window height in pixels.'
                               -w --width=[px] 'Window width in pixels.'
                               -s --seed=[seed] 'Seed for the RNG. Can be any string'")
                          .get_matches();

    let height = matches.value_of("height")
        .map(|h| h.parse::<u32>().expect("A natural number"))
        .unwrap_or(DEFAULT_HEIGHT);
    if height == 0 { panic!("Height must be > 0") }

    let width = matches.value_of("width")
        .map(|h| h.parse::<u32>().expect("A natural number"))
        .unwrap_or(DEFAULT_WIDTH);
    if width == 0 { panic!("Width must be > 0") }

    let seed_arg = matches.value_of("seed");
    let seed = seed_arg
        .map(|seed| {
            let mut hasher = DefaultHasher::new();
            seed.hash(&mut hasher);
            hasher.finish()
        })
        .unwrap_or(0);

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("mist", width, height)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas()
        .present_vsync()
        .build().unwrap();

    let texture_creator = canvas.texture_creator();
    let mut frame_texture = texture_creator.create_texture(
        PixelFormatEnum::RGBA8888,
        TextureAccess::Static,
        width,
        height
    ).expect("Error making frame texture");

    let rng = Xoroshiro64Star::seed_from_u64(seed);

    let sim = Arc::new(Mutex::new(SimulationBox::empty(width, height)));

    let mut framebuffer: Vec<u8> = vec![0; (4 * width * height) as usize];

    {
        let tlsim = sim.clone();
        std::thread::spawn(|| tick_loop(tlsim, rng));
    }

    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut frames = 0;
    let mut total_frames = 0;
    let mut total_elapsed = Duration::from_secs(0);
    let mut start = SystemTime::now();

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                Event::MouseButtonDown { x, y, .. } => {
                    println!("Click @ {}, {}", x, y);
                    let mut sim = sim.lock();
                    sim.draw_square((x as u32, y as u32), DEFAULT_BRUSH_SIZE, Particle::dust())
                }
                _ => {}
            }
        }

        {
            let sim = sim.lock();
            draw_pixels(&sim, &mut framebuffer);
        }

        frame_texture.update(None, &framebuffer, width as usize * 4).expect("Bad frame");
        canvas.copy(&frame_texture, None, None).expect("Bad copy");

        canvas.present();
        frames += 1;
        total_frames += 1;
        let loop_time = SystemTime::now();
        let elapsed = loop_time.duration_since(start).unwrap_or(Duration::from_secs(0));
        if elapsed > Duration::from_secs(FPS_UPDATE_DURATION) {
            total_elapsed += elapsed;
            start = loop_time;
            let fps = (frames as f64) / elapsed.as_secs_f64();
            let avg_fps = (total_frames as f64) / total_elapsed.as_secs_f64();
            frames = 0;
            println!("FPS = {:.2} (average {:.2})", fps, avg_fps);
        }

        // Sleep a few milliseconds to promote ticking
        std::thread::sleep(Duration::from_millis(TICK_FACTOR));
    }
}
