use std::convert::TryInto;
use std::ops::Index;
use std::mem::swap;
use rgb::{RGBA, RGBA8};
use rand::Rng;

const DUST_COLOR: RGBA8 = RGBA { r: 200, g: 100, b: 100, a: 255 };
const TRANSPARENT: RGBA8 = RGBA { r: 0, g: 0, b: 0, a: 0 };

/// A particle type
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ParticleType {
    /// A vacuum
    Empty,
    /// A particle of dust, affected by gravity
    Dust
}

/// A state of matter
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ParticleState {
    /// A solid
    Solid,
    /// A liquid
    Liquid,
    /// A gas
    Gas,
    /// An empty particle
    Void
}

/// A single particle
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Particle {
    /// The type of this particle
    pub ty: ParticleType,
    /// The (x, y) velocity of this particle
    pub v: (i16, i16)
}

impl Particle {
    pub fn empty() -> Particle { Particle {
        ty: ParticleType::Empty,
        v: (0, 0)
    } }
    pub fn dust() -> Particle { Particle {
        ty: ParticleType::Dust,
        v: (0, 0)
    } }
    pub fn base_color(&self) -> RGBA8 {
        use ParticleType::*;
        match self.ty {
            Empty => TRANSPARENT,
            Dust => DUST_COLOR
        }
    }
    pub fn state(&self) -> ParticleState {
        use ParticleType::*;
        use ParticleState::*;
        match self.ty {
            Empty => Void,
            Dust => Solid
        }
    }
}

/// A simulated box of particles
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct SimulationBox {
    particles: Vec<Particle>,
    height: u32,
    width: u32
}

impl SimulationBox {
    /// Create a new `SimulationBox` of a given size filled to a given capacity
    pub fn new(width: u32, height: u32, fill: Particle) -> SimulationBox {
        let capacity = (height * width) as usize;
        SimulationBox {
            particles: vec![fill; capacity],
            height,
            width
        }
    }
    /// Create an empty `SimulationBox` of a given size
    pub fn empty(width: u32, height: u32) -> SimulationBox {
        Self::new(width, height, Particle::empty())
    }
    /// Create a `SimulationBox` which is a random mixture of two particle types
    pub fn random_mix<R: Rng>(
        width: u32, height: u32,
        proportion: f64, base: Particle, other: Particle,
        rng: &mut R
    ) -> SimulationBox {
        let capacity = (height * width) as usize;
        let mut particles = Vec::with_capacity(capacity);
        for _ in 0..capacity {
            if rng.gen_bool(proportion) {
                particles.push(base)
            } else {
                particles.push(other)
            }
        }
        SimulationBox { particles, height, width }
    }

    /// Get a view of the particles in this `SimulationBox`
    pub fn particles<'a>(&'a self) -> SimulationParticles<'a> { SimulationParticles(self) }
    /// Get the height of this `SimulationBox`
    pub fn height(&self) -> u32 { self.height }
    /// Get the witdh of this `SimulationBox`
    pub fn width(&self) -> u32 { self.width }
    /// Put a particle at a given position. Get back what was there before, or Err if out of bounds
    pub fn put(&mut self, (x, y): (u32, u32), mut p: Particle) -> Result<Particle, ()> {
        if x >= self.height || y >= self.width { return Err(()) }
        swap(&mut p, &mut self.particles[(x + y*self.width) as usize]);
        Ok(p)
    }
    /// Draw a square of particles at a given position.
    pub fn draw_square(&mut self, (x, y): (u32, u32), side: u32, p: Particle) {
        let left = x.saturating_sub(side);
        let bot = y.saturating_sub(side);
        let right = x.saturating_add(side + 1).min(self.height);
        let top = y.saturating_add(side + 1).min(self.width);
        for x in left..right {
            for y in bot..top {
                self.particles[(x + y*self.width) as usize] = p
            }
        }
    }
    /// Turn a particle index to coordinates (in bounds). Get an error if they are out of bounds
    pub fn to_coords(&self, ix: usize) -> Result<(u32, u32), ()> {
        let ix: u32 = ix.try_into().map_err(|_| ())?;
        let x = ix as u32 % self.width;
        let y = ix as u32 / self.width;
        Ok((x, y))
    }
    /// Turn coordinates to a particle index (in bounds). Get an error if they are out of bounds
    pub fn to_ix(&self, (x, y): (u32, u32)) -> Result<usize, ()> {
        if x >= self.width || y >= self.height { Err(()) }
        else { Ok((x + y*self.width) as usize) }
    }
    /// Swap the particles at two positions.
    /// If one is out of bounds, replace the other with `p` and return what it was originally.
    /// If both are out of bounds, do nothing and return `None`
    fn swap_particles(&mut self, u: (u32, u32), v: (u32, u32), mut p: Particle)
    -> Result<(), Option<Particle>> {
        let i = self.to_ix(u);
        let j = self.to_ix(v);
        match (i, j) {
            (Err(_), Err(_)) => { Err(None) },
            (Err(_), Ok(i)) | (Ok(i), Err(_)) => {
                swap(&mut self.particles[i], &mut p);
                Err(Some(p))
            },
            (Ok(i), Ok(j)) => Ok(self.particles.swap(i, j))
        }
    }
    /// Handle a solid particle at a given index
    fn handle_solid<R: Rng>(
        &mut self,
        (x, y): (u32, u32), i: usize,
        mut p: Particle,
        g: (i32, i32),
        rng: &mut R
    ) {
        // Step 1: gravity integration
        let ggx = (g.0 / 65536) as i16;
        let ggy = (g.1 / 65536) as i16;
        let pgx = ((g.0 % 65536) as f64 / 65536.0).abs();
        let pgy = ((g.1 % 65536) as f64 / 65536.0).abs();
        let dgx = ggx.saturating_add(
            if rng.gen_bool(pgx) { if g.0 < 0 { -1 } else { 1 } } else { 0 }
        );
        let dgy = ggy.saturating_add(
            if rng.gen_bool(pgy) { if g.1 < 0 { -1 } else { 1 } } else { 0 }
        );
        p.v.0 = p.v.0.saturating_add(dgx);
        p.v.1 = p.v.1.saturating_add(dgy);

        // Step 2: update array
        self.particles[i] = p;

        // Step 3: transform velocity to motion probability.
        // 1 tick = 15 ms
        // Velocity unit = (1/256) px / tick
        let gmx = p.v.0 / 256;
        let gmy = p.v.1 / 256;
        let pmex = (p.v.0 % 256) as f64 / 256.0;
        let pmey = (p.v.0 % 256) as f64 / 256.0;
        let mx = if rng.gen_bool(pmex) { 1 } else { 0 } + gmx;
        let my = if rng.gen_bool(pmey) { 1 } else { 0 } + gmy;
        let (nx, ny) = (x as i32 + mx as i32, x as i32 + my as i32);
        let (nx, ny) = (
            if nx < 0 { self.width } else { nx as u32 },
            if ny < 0 { self.height } else { ny as u32 }
        );
        let _ = self.swap_particles((x, y), (nx, ny), Particle::empty());
    }
    /// Handle a liquid particle at a given index
    fn handle_liquid<R: Rng>(
        &mut self,
        (_x, _y): (u32, u32), _i: usize,
        _p: Particle,
        _g: (i32, i32),
        _rng: &mut R
    ) {
        //TODO
    }
    /// Handle a gaseous particle at a given index
    fn handle_gaseous<R: Rng>(
        &mut self,
        (_x, _y): (u32, u32), _i: usize,
        _p: Particle,
        _g: (i32, i32),
        _rng: &mut R) {
        //TODO
    }
    /// Tick with gravity in a given direction
    pub fn tick<R: Rng>(&mut self, g: (i32, i32), rng: &mut R) {
        let mut i = 0;
        for x in 0..self.width {
            for y in 0..self.height {
                use ParticleState::*;
                let p = self.particles[i];
                match p.state() {
                    Solid => self.handle_solid((x, y), i, p, g, rng),
                    Liquid => self.handle_liquid((x, y), i, p, g, rng),
                    Gas => self.handle_gaseous((x, y), i, p, g, rng),
                    Void => {} // Do nothing
                }
                i += 1;
            }
        }
    }
}

/// Get a view into the particles of a `SimulationBox`
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct SimulationParticles<'a>(pub &'a SimulationBox);

impl<'a> Index<usize> for SimulationParticles<'a> {
    type Output = [Particle];
    fn index(&self, ix: usize) -> &[Particle] {
        let w = self.0.width as usize;
        let start = w * ix;
        let end = start + w;
        &self.0.particles[start..end]
    }
}

impl<'a> SimulationParticles<'a> {
    pub fn iter(&self) -> impl Iterator<Item=&'a Particle> {
        self.0.particles.iter()
    }
    pub fn points(&self) -> impl Iterator<Item=((u32, u32), &'a Particle)> {
        let w = self.0.width;
        self.0.particles.iter().enumerate().map(move |(i, p)| {
            let x = (i as u32) % w;
            let y = (i as u32) / w;
            ((x, y), p)
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ParticleType::*;
    #[test]
    fn simple_construction_works_properly() {
        let dust = Particle { ty: Dust, v: (20, 30) };
        let emp = Particle::empty();
        let dust_box = SimulationBox::new(30, 40, dust);
        for particle in dust_box.particles().iter() {
            assert_eq!(dust, *particle);
        }
        let emp_box = SimulationBox::new(50, 20, emp);
        for particle in emp_box.particles().iter() {
            assert_eq!(emp, *particle);
        }
    }
}
